//the required form fields appear and disappear

const corpTogg = document.querySelector(`#corpTogg`);
const secondPersTogg = document.querySelector(`#secondPersTogg`);
const userCouponTogg = document.querySelector(`#userCouponTogg`);
const corpItem = document.querySelector(`.corp-item`);
const secondUser = document.querySelector(`.second-user`);
const couponItems = document.querySelector(`.coupon-items`);

corpTogg.addEventListener("click", () => {
    corpItem.classList.add('block-visible');
    if (!corpTogg.checked) {
        corpItem.classList.remove('block-visible');
    }
});

secondPersTogg.addEventListener("click", () => {
    secondUser.classList.add('block-flex-visible');
    if (!secondPersTogg.checked) {
        secondUser.classList.remove('block-flex-visible');
    }
});

userCouponTogg.addEventListener("click", () => {
    couponItems.classList.add('block-flex-visible');
    if (!userCouponTogg.checked) {
        couponItems.classList.remove('block-flex-visible');
    }
});

//coupon validation

const btnCoupon = document.querySelector(`#btnCoupon`);

function validateCoup() {
    const couponNum = document.querySelector(`#couponNum`);

    let valCoup = true;

    if (couponItems.style.display == "flex" && (!couponNum.value || couponNum.value.length < 3)) {
        couponNum.classList.add('error-border');
        errorMes.classList.add('block-visible');
        valCoup = false;
    } else {
        couponNum.classList.remove('error-border');
        errorMes.classList.remove('block-visible');
        console.log("validate completed");
    }
    return valCoup;
};

btnCoupon.addEventListener("click", (e) => {
    e.preventDefault();
    if (validateCoup(this)) {
        couponNum.submit(); //here you can write a sending to the server
    }
});

//form validation

const btnForm = document.querySelector(`#btnForm`);

function validate(form) {
    const corpName = document.querySelector(`#corpName`);
    const userEmail = document.querySelector(`#userEmail`);
    const firstName = document.querySelector(`#firstName`);
    const secondName = document.querySelector(`#secondName`);
    const addressStreet = document.querySelector(`#addressStreet`);
    const userCity = document.querySelector(`#userCity`);
    const secondPerson = document.querySelector(`#secondPerson`);
    const secondEmail = document.querySelector(`#secondEmail`);
    const userCard = document.querySelector(`#userCard`);
    const cardSecur = document.querySelector(`#cardSecur`);
    const errorMes = document.querySelector(`.error-message`);
    const errorImgscard = document.querySelector(`.error-img-scard`);
    const errorImgncard = document.querySelector(`.error-img-ncard`);
    const errorImglname = document.querySelector(`.error-img-lname`);
    const errorImgcity = document.querySelector(`.error-img-city`);
    const errorImgaddr = document.querySelector(`.error-img-addr`);
    const errorImgsname = document.querySelector(`.error-img-sname`);
    const errorImgfname = document.querySelector(`.error-img-fname`);
    const errorImgemail = document.querySelector(`.error-img-email`);
    const errorImgcompany = document.querySelector(`.error-img-company`);
    const errorImglemail = document.querySelector(`.error-img-lemail`);
    const tooltipHidCompany = document.querySelector(`.tooltip-hid-company`);
    const tooltipHidEmail = document.querySelector(`.tooltip-hid-email`);
    const tooltipHidFname = document.querySelector(`.tooltip-hid-fname`);
    const tooltipHidSname = document.querySelector(`.tooltip-hid-sname`);
    const tooltipHidAddr = document.querySelector(`.tooltip-hid-addr`);
    const tooltipHidCity = document.querySelector(`.tooltip-hid-city`);
    const tooltipHidLname = document.querySelector(`.tooltip-hid-lname`);
    const tooltipHidLemail = document.querySelector(`.tooltip-hid-lemail`);
    const tooltipHidNcard = document.querySelector(`.tooltip-hid-ncard`);
    const tooltipHidScard = document.querySelector(`.tooltip-hid-scard`);
   
 


    let result = true;
    let reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

    if (corpItem.classList.contains('block-visible') && (!corpName.value || corpName.value.length < 3)) {
        corpName.classList.add('error-border');
        errorMes.classList.add('block-visible');
        errorImgcompany.classList.add('error-img-visible');
        tooltipHidCompany.classList.add('tooltip');
        result = false;
    } else {
        corpName.classList.remove('error-border');
        errorMes.classList.remove('block-visible');
        errorImgcompany.classList.remove('error-img-visible');
        tooltipHidCompany.classList.remove('tooltip');
    }

    if (!userEmail.value) {
        userEmail.classList.add('error-border');
        errorMes.classList.add('block-visible');
        errorImgemail.classList.add('error-img-visible');
        tooltipHidEmail.classList.add('tooltip');
        result = false;
    } else if (reg.test(userEmail.value) == false) {
        userEmail.classList.add('error-border');
        errorMes.classList.add('block-visible');
        errorImgemail.classList.add('error-img-visible');
        tooltipHidEmail.classList.add('tooltip');
        result = false;
    } else {
        userEmail.classList.remove('error-border');
        errorMes.classList.remove('block-visible');
        errorImgemail.classList.remove('error-img-visible');
        tooltipHidEmail.classList.remove('tooltip');
    }

    if (!firstName.value || firstName.value.length < 3) {
        firstName.classList.add('error-border');
        errorMes.classList.add('block-visible');
        errorImgfname.classList.add('error-img-visible');
        tooltipHidFname.classList.add('tooltip');
        result = false;
    } else {
        firstName.classList.remove('error-border');
        errorMes.classList.remove('block-visible');
        errorImgfname.classList.remove('error-img-visible');
        tooltipHidFname.classList.remove('tooltip');
    }

    if (!secondName.value || secondName.value.length < 3) {
        secondName.classList.add('error-border');
        errorMes.classList.add('block-visible');
        errorImgsname.classList.add('error-img-visible');
        tooltipHidSname.classList.add('tooltip');
        result = false;
    } else {
        secondName.classList.remove('error-border');
        errorMes.classList.remove('block-visible');
        errorImgsname.classList.remove('error-img-visible');
        tooltipHidSname.classList.remove('tooltip');
    }

    if (!addressStreet.value || addressStreet.value.length < 3) {
        addressStreet.classList.add('error-border');
        errorMes.classList.add('block-visible');
        errorImgaddr.classList.add('error-img-visible');
        tooltipHidAddr.classList.add('tooltip');
        result = false;
    } else {
        addressStreet.classList.remove('error-border');
        errorMes.classList.remove('block-visible');
        errorImgaddr.classList.remove('error-img-visible');
        tooltipHidAddr.classList.remove('tooltip');
    }

    if (!userCity.value || userCity.value.length < 3) {
        userCity.classList.add('error-border');
        errorMes.classList.add('block-visible');
        errorImgcity.classList.add('error-img-visible');
        tooltipHidCity.classList.add('tooltip');
        result = false;
    } else {
        userCity.classList.remove('error-border');
        errorMes.classList.remove('block-visible');
        errorImgcity.classList.remove('error-img-visible');
        tooltipHidCity.classList.remove('tooltip');
    }

    if (secondUser.classList.contains('block-flex-visible') && (!secondPerson.value || secondPerson.value.length < 3)) {
        secondPerson.classList.add('error-border');
        errorMes.classList.add('block-visible');
        errorImglname.classList.add('error-img-visible');
        tooltipHidLname.classList.add('tooltip');
        result = false;
    } else {
        secondPerson.classList.remove('error-border');
        errorMes.classList.remove('block-visible');
        errorImglname.classList.remove('error-img-visible');
        tooltipHidLname.classList.remove('tooltip');
    }

    if (secondUser.classList.contains('block-flex-visible') && !secondEmail.value) {
        secondEmail.classList.add('error-border');
        errorMes.classList.add('block-visible');
        errorImglemail.classList.add('error-img-visible');
        tooltipHidLemail.classList.add('tooltip');
        result = false;
    } else if (secondUser.classList.contains('block-flex-visible') && reg.test(secondEmail.value) == false) {
        secondEmail.classList.add('error-border');
        errorMes.classList.add('block-visible');
        errorImglemail.classList.add('error-img-visible');
        tooltipHidLemail.classList.add('tooltip');
        result = false;
    } else {
        secondEmail.classList.remove('error-border');
        errorMes.classList.remove('block-visible');
        errorImglemail.classList.remove('error-img-visible');
        tooltipHidLemail.classList.remove('tooltip');
    }

    if (!userCard.value || userCard.value.length != 16) {
        userCard.classList.add('error-border');
        errorMes.classList.add('block-visible');
        errorImgncard.classList.add('error-img-visible');
        tooltipHidNcard.classList.add('tooltip');
        result = false;
    } else {
        userCard.classList.remove('error-border');
        errorMes.classList.remove('block-visible');
        errorImgncard.classList.remove('error-img-visible');
        tooltipHidNcard.classList.remove('tooltip');
    }

    if (!cardSecur.value || cardSecur.value.length != 3) {
        cardSecur.classList.add('error-border');
        errorMes.classList.add('block-visible');
        errorImgscard.classList.add('error-img-visible');
        tooltipHidScard.classList.add('tooltip');
        result = false;
    } else {
        cardSecur.classList.remove('error-border');
        errorMes.classList.remove('block-visible');
        errorImgscard.classList.remove('error-img-visible');
        tooltipHidScard.classList.remove('tooltip');
    }
    return result;
}

btnForm.addEventListener("click", (e) => {
    e.preventDefault();
    if (validate(this)) {
        form.submit();
    }
});